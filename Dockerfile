FROM php:7.2-fpm

ENV LANG ru_RU.UTF-8
ENV LC_ALL ru_RU.UTF-8
ENV COMPOSER_ALLOW_SUPERUSER 1 \
    TZ ${TZ:-"Europe/Moscow"}

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y locales \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && sed -i -e 's/# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=ru_RU.UTF-8

RUN apt-get update && apt-get install -y libpng-dev
RUN apt-get install -y openssl git unzip mc vim curl \
    libwebp-dev libjpeg62-turbo-dev libxpm-dev  libjpeg-dev libpng-dev libfreetype6-dev \
    libmemcached-dev libz-dev libpq-dev libssl-dev libmcrypt-dev \
    jpegoptim wget gnupg

COPY php.ini-development "$PHP_INI_DIR/php.ini-development"
COPY php.ini-production "$PHP_INI_DIR/php.ini-production"

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"


# Install NodeJS and npm
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs

RUN docker-php-ext-configure gd \
    --with-webp-dir \
    --with-jpeg-dir=/usr/include \
    --with-png-dir \
    --with-zlib-dir \
    --with-xpm-dir \
    --with-freetype-dir=/usr/include

RUN docker-php-ext-install pdo pdo_mysql gd  opcache exif sockets

RUN npm install -g yarn

# Install Composer
RUN wget http://getcomposer.org/installer \ 
    && php installer -- --install-dir=/usr/bin --filename=composer \ 
    && rm installer \ 
    && composer global require phploc/phploc 

WORKDIR /var/www/symfony
